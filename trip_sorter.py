from typing import NamedTuple, List


class TripSortError(Exception):
    pass


class Trip(NamedTuple):
    source: str
    destination: str
    description: str = ""


def _get_first_source(trips: List[Trip]) -> str:
    all_sources = set(i.source for i in trips)
    all_destinations = set(i.destination for i in trips)
    first_sourse = all_sources - all_destinations

    if len(first_sourse) != 1:
        raise TripSortError('discontinuous travel')

    return first_sourse.pop()


def trip_sort(trips: List[Trip]) -> List[Trip]:
    if not trips:
        return []

    trips_by_source = {i.source: i for i in trips}
    source = _get_first_source(trips)

    result = []
    while source in trips_by_source:
        trip = trips_by_source.pop(source)
        result.append(trip)
        source = trip.destination

    return result
