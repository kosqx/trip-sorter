from itertools import permutations

import pytest

from trip_sorter import Trip, TripSortError, trip_sort
 

@pytest.mark.parametrize('trips,sorted_trips', [
    (
        [],
        [],
    ),
    (
        [
            Trip('foo', 'bar'),
        ], [
            Trip('foo', 'bar'),
        ],
    ),
    (
        [
            Trip('bar', 'baz'),
            Trip('foo', 'bar'),
        ], [
            Trip('foo', 'bar'),
            Trip('bar', 'baz'),
        ],
    ),
])
def test_trip_sort_simple(trips, sorted_trips):
    assert trip_sort(trips) == sorted_trips


TRIPS = [
        Trip(
            'Madrid', 'Barcelona',
            'Take train 78A. Sit in seat 45B.'
        ),
        Trip(
            'Barcelona', 'Gerona Airport',
            'Take the airport bus. No seat assignment.'
        ),
        Trip(
            'Gerona Airport', 'Stockholm',
            'Take flight SK455. Gate 45B, seat 3A. '
            'Baggage drop at ticket counter 344.'
        ),
        Trip(
            'Stockholm', 'New York JFK',
            'Take flight SK22. Gate 22, seat 7B. '
            'Baggage will we automatically transferred from your last leg.'
        ),
    ]

    
@pytest.mark.parametrize('trips', permutations(TRIPS))
def test_trip_sort_all_permutations(trips):
    assert trip_sort(trips) == TRIPS


def test_trip_sort_discontinuous_travel():
    with pytest.raises(TripSortError):
        trip_sort([Trip('a', 'b'), Trip('c', 'd')])
