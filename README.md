# Trip sorter

## How to run it

```bash
# installation
python3.6 -m venv venv
source venv/bin/activate
python3.6 -m pip install pytest

# running tests
pytest

# running example code
python3.6 example.py
```


## Complexity

Given algorithm has complexity of `O(n)` (both time and memory) when `n` is number of boarding cards.
