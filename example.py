from trip_sorter import Trip, trip_sort 


trips = [
    Trip(
        'Stockholm', 'New York JFK',
        'Take flight SK22. Gate 22, seat 7B. '
        'Baggage will we automatically transferred from your last leg.'
    ),
    Trip(
        'Gerona Airport', 'Stockholm',
        'Take flight SK455. Gate 45B, seat 3A. '
        'Baggage drop at ticket counter 344.'
    ),
    Trip(
        'Barcelona', 'Gerona Airport',
        'Take the airport bus. No seat assignment.'
    ),
    Trip(
        'Madrid', 'Barcelona',
        'Take train 78A. Sit in seat 45B.'
    ),
]

for trip in trip_sort(trips):
    print(
        '* Travel from {trip.source} to {trip.destination}. '
        '{trip.description}'.format(trip=trip)
    )
print("* You have arrived at your final destination.")
